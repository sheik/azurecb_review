﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ServiceModel;
using Udooz.Net;
using System.Diagnostics;

namespace CbTestWeb.Controllers
{
    public class AuthorController : Controller
    {
        //
        // GET: /Author/

        public ActionResult Index()
        {
            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None, false);

            EndpointAddress myEndpoint = new EndpointAddress(
                "net.tcp://localhost:9001/HelloServiceEndpoints");

            try
            {
                ChannelFactory<IHelloService> helloChanFact = new ChannelFactory<IHelloService>(binding, myEndpoint);

                var circuitBreaker = CircuitBreaker<ChannelFactory<IHelloService>>
                                        .On(helloChanFact)
                                        .Execute<string>(cf =>
                                        {
                                            var helloClient = helloChanFact.CreateChannel();
                                            return helloClient.Greet("Sheik");
                                        }, msg => ViewBag.Message = msg,
                                        ex =>
                                        {
                                            ViewBag.Message = ex.Message;
                                        });
            }
            catch (Exception e)
            {
                Trace.WriteLine("An error occured trying to notify the instances: " + e.Message, "Warning");
            }

            return View();
        }

    }
}
