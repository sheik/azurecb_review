﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ServiceModel;
using Microsoft.WindowsAzure.ServiceRuntime;
using System.Diagnostics;
using Udooz.Net;

namespace CbTestWeb.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Welcome to ASP.NET MVC!";


            NetTcpBinding binding = new NetTcpBinding(SecurityMode.None, false);

            EndpointAddress myEndpoint = new EndpointAddress(
                "net.tcp://localhost:9001/HelloServiceEndpoints");

            CircuitBreaker<ChannelFactory<IHelloService>>
                .On(new ChannelFactory<IHelloService>(binding, myEndpoint))
                .Execute<string>(cf =>
                    {
                        var helloClient = cf.CreateChannel();
                        return helloClient.Greet("Udooz!");
                    }, msg => ViewBag.Message = msg,
                    ex =>
                    {
                        ViewBag.Message = ex.Message;
                    });

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}
