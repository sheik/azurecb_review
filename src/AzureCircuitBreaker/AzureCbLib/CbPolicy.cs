﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Udooz.Net
{
    public class CbPolicy
    {        
        public CbState State { get; internal set; }
        public Uri Resource { internal set; get; }
        public int FailureThreshold { internal set; get; }
        public TimeSpan TimeToReattempt { internal set; get; }
        public TimeSpan Timeout { internal set; get; }

        internal void Init(int failureThreshold, TimeSpan timeToReattempt, TimeSpan timeout)
        {
            this.FailureThreshold = failureThreshold;
            this.TimeToReattempt = timeToReattempt;
            this.Timeout = timeout;
        }

        internal CbPolicy()
        {

        }

        public CbPolicy(Uri resource, int failureThreshold, TimeSpan timeToReattempt, TimeSpan timeout, bool resourceHostOnly = false)
        {
            if (resourceHostOnly) this.Resource = new Uri(resource.Host);
            else this.Resource = resource;
            Init(failureThreshold, timeToReattempt, timeout);
        }

        public CbPolicy(Uri resource, int failureThreshold, TimeSpan timeToReattempt, TimeSpan timeout, string resourcePattern)
        {
            var match = Regex.Match(resource.AbsoluteUri, resourcePattern);
            if (match.Groups.Count > 0 && match.Groups[1].Success)
            {
                this.Resource = new Uri(match.Groups[1].Value);
            }
            else
            {
                throw new ArgumentException("The resource URL does not have match for the pattern");
            }
            Init(failureThreshold, timeToReattempt, timeout);
        }

        public override int GetHashCode()
        {
            return this.Resource.GetHashCode();
        }
    }
}
