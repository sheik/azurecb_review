﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Autofac;
using System.Threading.Tasks;

namespace Udooz.Net
{
    public class CircuitBreaker<T>
    {
        private CbPolicy policy;
        private static ICbPersistence persistence;
        private T origin;

        private CircuitBreaker(T resource)
        {
            //TODO: IoC should be applied
            if(persistence == null) persistence = new InMemoryCbPersistence();
            string resourceUri = string.Empty;
            this.origin = resource;

            //TODO: Type should be resolved outside of circuitbreaker with
            if (resource.GetType().Name.StartsWith("ChannelFactory"))
            {
                var obj = (ChannelFactory)(object)resource;
                resourceUri = obj.Endpoint.Address.Uri.AbsoluteUri;
            }
            this.policy = persistence.Get(resourceUri);
        }

        public static CircuitBreaker<T> On(T resource)
        {            
            return new CircuitBreaker<T>(resource);
        }

        public CircuitBreaker<T> Execute(Action<T> action, Action onSuccess, Action<Exception> onFail)
        {
            Exception exception = null;

            if (policy.State.Execute<T>(origin, action, out exception))
                onSuccess();
            else
                onFail(exception);
            return this;
        }

        public CircuitBreaker<T> Execute<R>(Func<T, R> func, Action<R> onSuccess, Action<Exception> onFail)
        {
            Exception exception = null;
            R retValue = default(R);

            if (policy.State.Execute<T, R>(origin, func, out retValue, out exception))
                onSuccess(retValue);
            else
                onFail(exception);
            return this;
        }
    }
}
