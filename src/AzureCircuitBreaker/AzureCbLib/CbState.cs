﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Udooz.Net
{
    public abstract class CbState
    {
        protected CbState(CbPolicy policy)
        {
            this.Policy = policy;
        }

        public int FailureCount { protected set; get; }
        public TimeSpan LastBrokenAt { protected set; get; }

        public CbPolicy Policy { private set; get; }

        public virtual bool Execute<T>(T origin, Action<T> action, out Exception exception)
        {
            exception = null;
            try
            {
                var task = Task.Factory.StartNew(() => action(origin));
                task.Wait(Policy.Timeout);
            }
            catch (AggregateException ae)
            {
                exception = ae.InnerException;
                return false;
            }
            return true;
        }
        public virtual bool Execute<T, R>(T origin, Func<T, R> func, out R retValue, out Exception exception)
        {
            retValue = default(R);
            exception = null;

            try
            {
                var task = Task.Factory.StartNew<R>(() => func(origin));
                task.Wait(Policy.Timeout);
                retValue = task.Result;
                return true;
            }
            catch (AggregateException ae)
            {
                exception = ae.InnerException;
                return false;
            }
        }
    }
}
