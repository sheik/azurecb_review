﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udooz.Net
{
    public class InMemoryCbPersistence : ICbPersistence
    {
        private static Dictionary<int, CbPolicy> cache = new Dictionary<int, CbPolicy>();

        public InMemoryCbPersistence() { }

        public void Upsert(CbPolicy policy)
        {
            if (cache.ContainsKey(policy.GetHashCode())) cache.Remove(policy.GetHashCode());
            cache.Add(policy.GetHashCode(), policy);
        }

        public CbPolicy Get(string resource)
        {
            var key = new Uri(resource).GetHashCode();
            if (cache.ContainsKey(key)) return cache[key];
            return null;
        }
    }
}
