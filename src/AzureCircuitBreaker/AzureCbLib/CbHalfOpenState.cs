﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Udooz.Net
{
    public class CbHalfOpenState : CbState
    {
        public CbHalfOpenState(CbPolicy policy)
            : base(policy)
        {

        }

        public override bool Execute<T>(T origin, Action<T> action, out Exception exception)
        {
            bool result = base.Execute<T>(origin, action, out exception);
            if (result) CloseTrip();
            else BreakTrip(exception);
            
            return result;
        }

        public override bool Execute<T, R>(T origin, Func<T, R> func, out R retValue, out Exception exception)
        {
            bool result = base.Execute<T, R>(origin, func, out retValue, out exception);
            if (result) CloseTrip();
            else BreakTrip(exception);
            return result;
        }

        private void BreakTrip(Exception lastException)
        {
            this.Policy.State = new CbOpenState(this.Policy, lastException);
        }

        private void CloseTrip()
        {
            this.Policy.State = new CbClosedState(this.Policy);
        }
    }
}
