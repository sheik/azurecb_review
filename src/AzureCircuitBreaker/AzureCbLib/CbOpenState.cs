﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udooz.Net
{
    public class CbOpenState : CbState
    {
        private Exception lastException;

        public CbOpenState(CbPolicy policy, Exception lastException)
            : base(policy)
        {
            LastBrokenAt = new TimeSpan(DateTime.Now.Ticks);
            this.lastException = lastException;
        }

        public override bool Execute<T>(T origin, Action<T> action, out Exception exception)
        {
            if (!Timeout())
            {
                exception = lastException;
                return false;
            }
            else
            {
                ResumeAttempt();
                return this.Policy.State.Execute<T>(origin, action, out exception);
            }
        }

        public override bool Execute<T, R>(T origin, Func<T, R> func, out R retValue, out Exception exception)
        {
            if (!Timeout())
            {
                exception = lastException;
                retValue = default(R);
                return false;
            }
            else
            {
                ResumeAttempt();
                return this.Policy.State.Execute<T, R>(origin, func, out retValue, out exception);
            }
        }

        private bool Timeout()
        {
            var now = new TimeSpan(DateTime.Now.Ticks);
            return now.Subtract(LastBrokenAt) >= this.Policy.TimeToReattempt;
        }

        private void ResumeAttempt()
        {            
            LastBrokenAt = TimeSpan.Zero;
            this.Policy.State = new CbHalfOpenState(this.Policy);
        }
    }
}
