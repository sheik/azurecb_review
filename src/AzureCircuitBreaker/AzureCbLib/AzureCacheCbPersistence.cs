﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.ApplicationServer.Caching;

namespace Udooz.Net
{
    public class AzureCacheCbPersistence : ICbPersistence
    {
        private static DataCacheFactory cacheFactory;
        private static DataCacheFactoryConfiguration cacheFacConfig;
        private static DataCache acbCache;

        public AzureCacheCbPersistence()
        {
            if (cacheFacConfig == null)
            {
                cacheFacConfig = new DataCacheFactoryConfiguration("acb");
            }
            if (cacheFactory == null)
            {
                cacheFactory = new DataCacheFactory(cacheFacConfig);
            }
            cacheFactory.GetCache("acb");
        }

        public void Upsert(CbPolicy policy)
        {
            acbCache.Put(policy.GetHashCode().ToString(), policy);
        }

        public CbPolicy Get(string resource)
        {
            var policy = acbCache.Get(resource.GetHashCode().ToString());
            if (policy != null) return policy as CbPolicy;
            return null;
        }
    }
}
