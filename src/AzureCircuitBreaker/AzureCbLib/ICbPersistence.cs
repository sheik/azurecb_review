﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udooz.Net
{
    public interface ICbPersistence
    {
        void Upsert(CbPolicy policy);
        CbPolicy Get(string resource);
    }
}
