﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace Udooz.Net
{
    public static class EndpointExtensions
    {
        public static string GetEndpointAddress<T>(this ChannelFactory<T> obj)
        {
            return obj.Endpoint.Address.Uri.AbsoluteUri;
        }
    }
}
