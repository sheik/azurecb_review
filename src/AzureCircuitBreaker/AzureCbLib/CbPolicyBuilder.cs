﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udooz.Net
{
    public class CbPolicyBuilder
    {
        private CbPolicy policy;
        private ICbPersistence persistence;

        private CbPolicyBuilder()
        {
            policy = new CbPolicy();
            policy.State = new CbClosedState(policy);
            policy.Timeout = TimeSpan.FromSeconds(30);
            policy.FailureThreshold = 1;
            policy.TimeToReattempt = TimeSpan.FromMinutes(1);
            persistence = new InMemoryCbPersistence();
        }

        public static CbPolicyBuilder For(string resource)
        {           
            var builder = new CbPolicyBuilder();
            builder.policy.Resource = new Uri(resource);
            return builder;
        }

        public CbPolicyBuilder ThenFor(string resource)
        {
            policy = new CbPolicy();
            policy.State = new CbClosedState(policy);
            policy.Resource = new Uri(resource);
            return this;
        }

        public CbPolicyBuilder Timeout(TimeSpan timeout)
        {
            policy.Timeout = timeout;
            return this;
        }

        public CbPolicyBuilder MaxFailure(int count)
        {
            policy.FailureThreshold = count;
            return this;
        }

        public CbPolicyBuilder OpenTripFor(TimeSpan period)
        {
            policy.TimeToReattempt = period;
            return this;
        }

        public void Do()
        {
            persistence.Upsert(policy);
        }
    }
}
