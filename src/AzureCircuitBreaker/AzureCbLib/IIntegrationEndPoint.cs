﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udooz.Net
{
    public interface IIntegrationEndPoint
    {
        string Uri { get; }
    }
}
