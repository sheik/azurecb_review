﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Udooz.Net
{
    public class CbClosedState : CbState
    {
        public CbClosedState(CbPolicy policy)
            : base(policy)
        {
            ResetFailureCount();
        }

        public override bool Execute<T>(T origin, Action<T> action, out Exception exception)
        {
            bool result = base.Execute<T>(origin, action, out exception);
            if (result) ResetFailureCount();
            else CheckThreashold(exception);            
            return result;
        }

        public override bool Execute<T, R>(T origin, Func<T, R> func, out R retValue, out Exception exception)
        {
            bool result = base.Execute<T, R>(origin, func, out retValue, out exception);
            if (result) ResetFailureCount();
            else CheckThreashold(exception);
            return result;
        }

        private void ResetFailureCount()
        {
            FailureCount = 0;
            LastBrokenAt = TimeSpan.Zero;
        }

        private void CheckThreashold(Exception exception)
        {
            if (++FailureCount < this.Policy.FailureThreshold)
                this.Policy.State = this;
            else
                this.Policy.State = new CbOpenState(this.Policy, exception);
        }
    }
}
