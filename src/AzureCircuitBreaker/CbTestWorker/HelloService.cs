﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace CbTestWorker
{
    [ServiceContract]
    public interface IHelloService
    {

        [OperationContract]
        string Greet(string name);
    }


    public class HelloService : IHelloService
    {

        public string Greet(string name)
        {
            return string.Format("Hello, {0}", name);
        }

    } 
}
